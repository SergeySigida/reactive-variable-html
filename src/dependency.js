const PREFIX_ID = 'reactive_var_'
const ATTRIBUTE_DATA_VAR = 'data-reactive-var'
const PREFIX_COMPUTED = 'computed_'
const ATTRIBUTE_DATA_VAR_COMPUTED = 'data-reactive-var-computed'

export default class Dep {
	constructor(data) {
		this.data = data.data() || {}
		this.computeds = data.computed || {}
		this.init()
		this.processingDOM()
	}
	init() {
		const context = this

		Object.keys(this.data).forEach(key => {
			let val = this.data[key]
			Object.defineProperty(this.data, key, {
				get() {
					console.log('get', key)
					return val
				},
				set(newVal) {
					console.log('set', key)
					val = newVal
					context.notify()
					context.renderElem(PREFIX_ID, key, val)
				},
			})
		})
	}
	notify() {
		console.log('notify')
		Object.keys(this.computeds).forEach(sub => {
			const val = this.computeds[sub].call(this.data)
			this.renderElem(PREFIX_ID + PREFIX_COMPUTED, sub, val)
		})
	}
	processingDOM() {
		Object.keys(this.data).forEach(key => {
			const $elem = document.querySelector(`[${ATTRIBUTE_DATA_VAR}="${key}"]`)

			if ($elem) {
				$elem.id = PREFIX_ID + key
				$elem.removeAttribute(ATTRIBUTE_DATA_VAR)
				$elem.innerText = this.data[key]
			}
		})
		Object.keys(this.computeds).forEach(key => {
			const $elem = document.querySelector(`[${ATTRIBUTE_DATA_VAR_COMPUTED}="${key}"]`)

			if ($elem) {
				$elem.id = PREFIX_ID + PREFIX_COMPUTED + key
				$elem.removeAttribute(ATTRIBUTE_DATA_VAR_COMPUTED)
				$elem.innerText = this.computeds[key].call(this.data)
			}
		})
	}
	renderElem(prefix, key, val) {
		const $elem = document.getElementById(prefix + key)
		if ($elem) $elem.innerText = val
	}
}