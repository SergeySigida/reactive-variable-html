# Reactive variables in HTML
Проект дает возможность использовать реактивные переменные в коде html

В js коде нужно проинициализировать class Dep и передать объект параметров

data: функция возвращающая обект переменных

computed: объект вычисляемых функций

``` javascript
import { Dep } from '../src/dependency'

const depClass = new Dep({
  data: () => ({
    val1: 10,
    val2: 24
  }),
  computed: {
    total() {
      return this.val1 * this.val2
    }
  }
})
```
Вывод переменной в HTML
```html
<span data-reactive-var="val1"></span>
```

Вывод вычисляемого значения
```html
<p>sum: <span data-reactive-var-computed="total"></span></p>
```