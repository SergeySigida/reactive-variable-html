import { Dep } from '../src/dependency'

window.depClass = new Dep({
  data: () => ({
    val1: 10,
    val2: 24,
    name: '',
    surname: ''
  }),
  computed: {
    total() {
      return this.val1 * this.val2
    },
    fullName() {
      return `${this.name} ${this.surname}`
    }
  }
})

window.changeVal = function (prop, val) {
  depClass.data[prop] = val
}